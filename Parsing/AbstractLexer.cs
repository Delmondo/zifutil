using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Zif.Util.Parsing
{
    public abstract class AbstractLexer : IEnumerable<Token>
    {
        protected static Condition FoundWhitespace = (AbstractLexer lexer) =>
        {
            return Char.IsWhiteSpace(lexer.CurrentChar);
        };

        protected Stream Stream;
        protected StreamReader StreamReader;

        protected int CurrentByte = -1;
        protected int NextByte
        {
            get
            {
                var b = Stream.ReadByte();
                Stream.Seek(-1L, SeekOrigin.Current);

                return b;
            }
        }

        protected virtual LexerPipelineMap Pipeline { get; set; } = new LexerPipelineMap();

        public ulong Line { get; protected set; } = 1UL;
        public ulong Column { get; protected set; } = 1UL;

        public char CurrentChar
        {
            get => (char)CurrentByte;
        }

        public char NextChar
        {
            get => (char)NextByte;
        }

        protected AbstractLexer(Stream stream)
        {
            Stream = stream;
            StreamReader = new StreamReader(stream);
            CurrentByte = Stream.ReadByte();

            Pipeline.PushBack
            (
                (FoundWhitespace, () => { SkipWhitespace(); return null; })
            );
        }

        protected void Move(ulong skip = 1L)
        {
            for (ulong i = 0; i < skip; ++i)
            {
                CurrentByte = Stream.ReadByte();
                ++Column;

                if (CurrentChar == '\n')
                {
                    Column = 1UL;
                    ++Line;
                }
            }
        }

        protected string ReadChars(ulong amount)
        {
            var chars = new List<char>();

            for (ulong i = 0; i < amount; ++i)
            {
                chars.Add(CurrentChar);
                Move();
            }

            return new string(chars.ToArray());
        }

        protected string ReadWhile(Func<bool> f)
        {
            var chars = new List<char>();

            while (f() && CurrentByte != -1)
            {
                chars.Add(CurrentChar);
                Move();
            }

            return new string(chars.ToArray());
        }

        protected void SkipWhitespace()
        {
            ReadWhile(() => Char.IsWhiteSpace(CurrentChar));
        }

        protected Token Next()
        {
            foreach (var b in Pipeline)
            {
                if ((bool)b.Item1.DynamicInvoke(new object[] { this }))
                {
                    var token = b.Item2.Invoke();

                    if (token != null)
                    {
                        return token;
                    }
                }
            }

            throw new UnrecognizedCharacterException(this, $"Unexpected character \"{CurrentChar}\" (character 0x{CurrentByte.ToString("X")}).");
        }

        protected string NextLine()
        {
            return StreamReader.ReadLine();
        }

        protected bool HasNext()
        {
            SkipWhitespace(); // this is to not read past stream end when there's blank newlines at the end of the file
            return CurrentByte != -1;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<Token> GetEnumerator()
        {
            while (HasNext())
            {
                yield return Next();
            }
        }
    }
}
