using System;
using System.IO;

namespace Zif.Util.Parsing
{
    public abstract class AbstractLexerComments : AbstractLexer
    {
        protected Condition FoundMultilineComment = (AbstractLexer lexer) =>
        {
            return lexer.CurrentChar == '/' && lexer.NextChar == '*';
        };

        protected static Condition FoundSingleLineComment = (AbstractLexer lexer) =>
        {
            return lexer.CurrentChar == '/' && lexer.NextChar == '/';
        };

        protected AbstractLexerComments(Stream stream) : base(stream)
        {
            Pipeline.PushBack
            (
                (FoundMultilineComment, () => { SkipMultilineComment(); return null; })
            );

            Pipeline.PushBack
            (
                (FoundSingleLineComment, () => { SkipSingleLineComment(); return null; })
            );
        }

        protected void SkipMultilineComment()
        {
            while (true)
            {
                Move();
                if (CurrentChar == '*' && NextChar == '/')
                {
                    Move(2L);
                    break;
                }
            }

            SkipWhitespace();
        }

        protected void SkipSingleLineComment()
        {
            ReadWhile(() => CurrentChar != '\n');
            Move();
        }
    }
}
