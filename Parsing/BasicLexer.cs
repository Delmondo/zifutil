using System;
using System.Collections.Generic;
using System.IO;

namespace Zif.Util.Parsing
{
    public class BasicLexer : AbstractLexerComments
    {
        public enum TokenType
        {
            None,
            Word,
            Number,
            String,
            Symbol,
            Punctuation
        }

        protected static Condition FoundLetter = (AbstractLexer lexer) =>
        {
            return Char.IsLetter(lexer.CurrentChar);
        };

        protected static Condition FoundNumber = (AbstractLexer lexer) =>
        {
            return Char.IsDigit(lexer.CurrentChar) || lexer.CurrentChar == '-';
        };

        protected static Condition FoundQuote = (AbstractLexer lexer) =>
        {
            return lexer.CurrentChar == '\"';
        };

        protected static Condition FoundSymbol = (AbstractLexer lexer) =>
        {
            return Char.IsSymbol(lexer.CurrentChar);
        };

        protected static Condition FoundPunctuation = (AbstractLexer lexer) =>
        {
            return Char.IsPunctuation(lexer.CurrentChar);
        };

        public BasicLexer(Stream stream) : base(stream)
        {
            Pipeline.PushBack
            (
                (FoundLetter, () => { return new Token(ReadWord(), TokenType.Word); })
            );

            Pipeline.PushBack
            (
                (FoundNumber, () => { return new Token(ReadNumber(), TokenType.Number); })
            );

            Pipeline.PushBack
            (
                (FoundQuote, () => { return new Token(ReadStringLiteral(), TokenType.String); })
            );

            Pipeline.PushBack
            (
                (FoundSymbol, () => { return new Token(ReadChars(1UL), TokenType.Symbol); })
            );

            Pipeline.PushBack
            (
                (FoundPunctuation, () => { return new Token(ReadChars(1UL), TokenType.Punctuation); })
            );
        }

        protected string ReadWord()
        {
            return ReadWhile(() => Char.IsLetterOrDigit(CurrentChar));
        }

        protected string ReadNumber()
        {
            string sign = "";

            if (CurrentChar == '-')
            {
                sign = "-";
                Move();
            }

            string exponent = ReadWhile(() => Char.IsDigit(CurrentChar));
            string mantissa = "";

            if (CurrentChar == '.')
            {
                mantissa += ".";
                Move();
                mantissa += ReadWhile(() => Char.IsDigit(CurrentChar));
            }

            return sign + exponent + mantissa;
        }

        protected string ReadStringLiteral()
        {
            var chars = new List<char>();

            while (true)
            {
                Move();

                if (CurrentChar == '\n')
                {
                    throw new UnterminatedStringLiteralException(this, "Unterminated string literal.");
                }

                if (CurrentChar == '\"')
                {
                    Move();
                    break;
                }

                chars.Add(CurrentChar);
            }

            return new string(chars.ToArray());
        }
    }
}
