using System;

namespace Zif.Util.Parsing
{
    public delegate bool Condition(AbstractLexer lexer);
}
