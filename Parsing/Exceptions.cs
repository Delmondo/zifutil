using System;

namespace Zif.Util.Parsing
{
    public class LexerException : Exception
    {
        protected AbstractLexer Lexer;
        protected string ErrorMessage;

        public LexerException(AbstractLexer lexer)
        {
            Lexer = lexer;
            ErrorMessage = $"{Lexer.Line}:{Lexer.Column}: ";
        }

        public LexerException(AbstractLexer lexer, string message) : this(lexer)
        {
            ErrorMessage += message;
        }

        public LexerException(AbstractLexer lexer, string message, Exception innerException) : base(message, innerException)
        {
            Lexer = lexer;
            ErrorMessage = $"{Lexer.Line}:{Lexer.Column}: {message}";
        }
    }

    public class UnrecognizedCharacterException : LexerException
    {
        public UnrecognizedCharacterException(AbstractLexer lexer) : base(lexer) { }
        public UnrecognizedCharacterException(AbstractLexer lexer, string message) : base(lexer, message) { }
        public UnrecognizedCharacterException(AbstractLexer lexer, string message, Exception innerException) : base(lexer, message, innerException) { }
    }

    public class UnterminatedStringLiteralException : LexerException
    {
        public UnterminatedStringLiteralException(AbstractLexer lexer) : base(lexer) { }
        public UnterminatedStringLiteralException(AbstractLexer lexer, string message) : base(lexer, message) { }
        public UnterminatedStringLiteralException(AbstractLexer lexer, string message, Exception innerException) : base(lexer, message, innerException) { }
    }
}
