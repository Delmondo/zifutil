using System;
using System.Collections.Generic;

namespace Zif.Util.Parsing
{
    public sealed class LexerPipelineMap
    {
        private List<(Condition, Func<Token>)> _map = new List<(Condition, Func<Token>)>();

        public override bool Equals(object obj)
        {
            return _map.Equals(obj);
        }

        public override int GetHashCode()
        {
            return _map.GetHashCode();
        }

        public void AddAfter(Condition cond, (Condition, Func<Token>) value)
        {
            var found = Find(cond);

            if (found == null)
            {
                return;
            }
        }

        public void PushFront((Condition cond, Func<Token>) value)
        {
            _map.Insert(0, value);
        }

        public void PushBack((Condition cond, Func<Token>) value)
        {
            _map.Add(value);
        }

        public void Clear()
        {
            _map.Clear();
        }

        public bool Contains(Condition cond)
        {
            return Find(cond) != null;
        }

        public void CopyTo((Condition, Func<Token>)[] array, int index)
        {
            _map.CopyTo(array, index);
        }

        public (Condition cond, Func<Token> value)? Find(Condition cond)
        {
            foreach (var t in _map)
            {
                if (t.Item1 == cond)
                {
                    return t;
                }
            }

            return null;
        }

        public (Condition, Func<Token>)? FindLast(Condition cond)
        {
            for (int i = _map.Count - 1; i >= 0; --i)
            {
                if (_map[i].Item1 == cond)
                {
                    return _map[i];
                }
            }

            return null;
        }

        public IEnumerator<(Condition, Func<Token>)> GetEnumerator()
        {
            return _map.GetEnumerator();
        }

        public bool Remove(Condition cond)
        {
            var found = Find(cond);

            if (found != null)
            {
                _map.Remove(found.GetValueOrDefault());
                return true;
            }

            return false;
        }

        public void RemoveFront()
        {
            _map.RemoveAt(0);
        }

        public void RemoveBack()
        {
            _map.RemoveAt(_map.Count - 1);
        }
    }
}
