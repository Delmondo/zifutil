using System;

namespace Zif.Util.Parsing
{
    public class Token
    {
        public string Text { get; protected set; }
        public Enum Type { get; protected set; }

        public Token(string text, Enum type)
        {
            Text = text;
            Type = type;
        }
    }
}
